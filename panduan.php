<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<?php include "includes/head.php";?>

<body>
<?php include "includes/header.php";?>
<div class="header_page hp_panduan">
	<div class="container">
		<h2>Panduan Gizi Seimbang</h2>
	</div>
</div>
<div class="clearfix pt30"></div>
<div class="container">
	<div class="right detail">
		<div class="breadcrumb">
			<img src="img/ico_home.jpg" alt="home">
			<span>
				/ Panduan Gizi Seimbang
			</span>
		</div>
		<div class="clearfix pt20"></div>
		<h1>Panduan Gizi Seimbang</h1>
		<div class="pic"><img src="img/img2.jpg" alt="gambar"></div>
		<div class="text_detail">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sapien risus, viverra a commodo sit amet, mollis vitae quam. Cras vestibulum, odio et lacinia vestibulum, neque nunc rutrum dui, eget dapibus dui lorem et mi. Phasellus finibus faucibus libero id iaculis. Fusce id nulla enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec mattis mauris. Integer placerat blandit nibh, ut dapibus enim dignissim non.
			<br><br>
			Sed sodales nibh mi, <a href="#">quis blandit ex imperdiet non.</a> Donec tristique magna sed arcu ornare euismod. Mauris eu ullamcorper tellus. Nulla mattis est vel dapibus elementum. Vivamus sit amet tellus ipsum. Etiam tempor massa eget tortor aliquam, sed fermentum risus suscipit. Phasellus congue ac enim eu facilisis. Vivamus at ipsum ut arcu luctus lobortis. Praesent euismod purus mi, sed dapibus eros ultrices at. In porttitor porttitor urna, eget dapibus enim gravida at. Aliquam luctus imperdiet diam, non convallis risus aliquam in.
			<div class="sisip">Sed ut velit tempor, sollicitudin dui ut, vulputate orci. Integer eget felis viverra, vehicula orci et, posuere urna. Integer eleifend massa id ipsum ornare, sed pulvinar urna aliquet.</div>
			Sed ut velit tempor, sollicitudin dui ut, vulputate orci. Integer eget felis viverra, vehicula orci et, posuere urna. Integer eleifend massa id ipsum ornare, sed pulvinar urna aliquet. Sed ex dolor, tincidunt quis ullamcorper vel, mattis non arcu. Sed hendrerit tortor vel elit facilisis, non porta lectus convallis. Nulla vulputate, tellus eget tincidunt luctus, risus massa tempor augue, a tempus arcu nisl sed dolor. Morbi non sapien id justo consequat convallis vel quis mauris. Phasellus hendrerit tristique mi, vel pulvinar tellus viverra ac. Fusce ullamcorper mi eget risus tempus semper. Integer lobortis vitae enim id faucibus. Fusce accumsan nisi quis velit euismod, eget molestie nunc pellentesque.
			<br><br>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc urna neque, sodales ac quam sagittis, rutrum laoreet sem. Integer quis porta eros, vitae pharetra lorem. Proin rutrum vitae mauris sit amet luctus. Sed at est ligula. Etiam dapibus lorem sem, vitae tincidunt odio rutrum tincidunt. Pellentesque porta luctus tincidunt. Nulla neque orci, dictum sed suscipit quis, consequat sit amet leo. Integer nec congue nulla, quis porttitor quam. Praesent congue ex et viverra dictum.
		</div>
		<div class="share">
			<a href="#"><img src="img/share_fb.png" alt="share facebook"></a>
			<a href="#"><img src="img/share_tw.png" alt="share twiiter"></a>
		</div>
	</div>
	<div class="left">
		<div class="box1">
			<a href="#"><h2>Pentingnya Buah & Sayur</h2></a>
			<a href="#"><h2>Sumber Gizi Utama</h2></a>
			<a href=""><h2>Buah Sayur & Perkembangan si Kecil</h2></a>
		</div>
		<div class="clearfix pt30"></div>
		<div class="box2">
			<div class="box_title"><h4>Tips terbaru</h4></div>
			<ul class="list_box2">
				<li>
					<h5><a href="#">Resep sayuran kreatif</a></h5>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sapien risus
				</li>
				<li>
					<h5><a href="#">Membuat sayuran mudah & menarik</a></h5>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sapien risus
				</li>
				<li>
					<h5><a href="#">Jus sehat bagi si kecil</a></h5>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sapien risus
				</li>
				<li>
					<h6><a href="tips.php">lihat tips lainnya</a></h6>
				</li>
			</ul>
		</div>
		<div class="clearfix pt30"></div>
	</div>
	<div class="clearfix"></div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
	
</body>
</html>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<?php include "includes/head.php";?>

<body id="top">
<?php include "includes/header.php";?>
<div class="page1" id="fakta">
	<div class="container">
		<div class="hl">
			<div id="hl">
				<div class="list_hl">
					<div class="text">
						<h2>
							<a href="fakta.php">
								Fakta
								Buah dan Sayur
							</a>
						</h2>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. Cras consectetur arcu porttitor tortor mattis, scelerisque accumsan magna luctus. Fusce auctor, ante et cursus pulvinar, ligula mauris suscipit magna.
					</div>
					<div class="hl_r">
						<div class="pic">
							<img src="img/hl1.png" alt="detail pic">
						</div>
						<div class="plus" style="left:57%; top:72%;">
							<span><img src="img/ico_plus.png" alt="view description"></span>
							<div class="box_p">
								<h3>BUAH</h3>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. 
							</div>
						</div>
						<div class="plus" style="left:63%; top:35%;">
							<span><img src="img/ico_plus.png" alt="view description"></span>
							<div class="box_p">
								<h3>BUAH</h3>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. 
							</div>
						</div>
					</div>
				</div>
				<div class="list_hl">
					<div class="text">
						<h2>
							<a href="fakta.php">
								Fakta
								Buah dan Sayur
							</a>
						</h2>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. Cras consectetur arcu porttitor tortor mattis, scelerisque accumsan magna luctus. Fusce auctor, ante et cursus pulvinar, ligula mauris suscipit magna.
					</div>
					<div class="hl_r">
						<div class="pic">
							<img src="img/hl1.png" alt="headline">
						</div>
						<div class="plus" style="left:57%; top:72%;">
							<span><img src="img/ico_plus.png" alt="view description"></span>
							<div class="box_p">
								<h3>BUAH</h3>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. 
							</div>
						</div>
						<div class="plus" style="left:63%; top:35%;">
							<span><img src="img/ico_plus.png" alt="view description"></span>
							<div class="box_p">
								<h3>BUAH</h3>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. 
							</div>
						</div>
					</div>
				</div>
				<div class="list_hl">
					<div class="text">
						<h2>
							<a href="fakta.php">
								Fakta
								Buah dan Sayur
							</a>
						</h2>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. Cras consectetur arcu porttitor tortor mattis, scelerisque accumsan magna luctus. Fusce auctor, ante et cursus pulvinar, ligula mauris suscipit magna.
					</div>
					<div class="hl_r">
						<div class="pic">
							<img src="img/hl1.png" alt="healine">
						</div>
						<div class="plus" style="left:57%; top:72%;">
							<span><img src="img/ico_plus.png" alt="view description"></span>
							<div class="box_p">
								<h3>BUAH</h3>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. 
							</div>
						</div>
						<div class="plus" style="left:63%; top:35%;">
							<span><img src="img/ico_plus.png" alt="view description"></span>
							<div class="box_p">
								<h3>BUAH</h3>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. 
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="pagination" id="pagnation"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="page2" id="panduan">
	<div class="container container2">
		<h2 class="title"><a href="panduan.php">Panduan Gizi Seimbang</a></h2>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium dolor at dolor tempus, vel pretium dolor suscipit. Cras consectetur arcu porttitor tortor mattis, scelerisque accumsan magna luctus. Fusce auctor, ante et cursus pulvinar, ligula mauris suscipit magna.
	</div>
	<div id="tips"></div>
</div>
<div class="page3">
	<div class="container t-center">
		<h2 class="title">Tips Buah, Sayur & Si Kecil</h2>
		<div class="clearfix pt30"></div>
		<div class="slide2">
			<div id="carousel">
				<div>
					<a href="tips.php">
						<span class="pic"><img src="img/tip1.jpg" alt="buah sayur" /></span>
						<h3>Ajari anak memasak sayuran</h3>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
					</a>
				</div>
				<div>
					<a href="tips.php">
						<span class="pic"><img src="img/tip2.jpg" alt="buah sayur" /></span>
						<h3>Ajari anak memasak sayuran</h3>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
					</a>
				</div>
				<div>
					<a href="tips.php">
						<span class="pic"><img src="img/tip3.jpg" alt="buah sayur" /></span>
						<h3>Ajari anak memasak sayuran</h3>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
					</a>
				</div>
				<div>
					<a href="tips.php">
						<span class="pic"><img src="img/tip4.jpg" alt="buah sayur" /></span>
						<h3>Ajari anak memasak sayuran</h3>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
					</a>
				</div>
				<div>
					<a href="tips.php">
						<span class="pic"><img src="img/tip1.jpg" alt="buah sayur" /></span>
						<h3>Ajari anak memasak sayuran</h3>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
					</a>
				</div>
				<div>
					<a href="tips.php">
						<span class="pic"><img src="img/tip2.jpg" alt="buah sayur" /></span>
						<h3>Ajari anak memasak sayuran</h3>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
					</a>
				</div>
				<div>
					<a href="tips.php">
						<span class="pic"><img src="img/tip3.jpg" alt="buah sayur" /></span>
						<h3>Ajari anak memasak sayuran</h3>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
					</a>
				</div>
				<div>
					<a href="tips.php">
						<span class="pic"><img src="img/tip4.jpg" alt="buah sayur" /></span>
						<h3>Ajari anak memasak sayuran</h3>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<img src="img/nav_left.png" alt="" id="slide2_prev" class="nav_slide2_left">
		<img src="img/nav_right.png" alt="" id="slide2_next" class="nav_slide2_right">
		<div class="clearfix"></div>
		<div class="pagination pagination2" id="pagnation2"></div>
	</div>
</div>
<div class="page4" id="hubungi">
	<div class="container">
		<img src="img/img_hub.jpg" alt="hubungi kami" class="fl">
		<div class="fr">
			<h2 class="title">Hubungi Kami</h2>
			<form action="#" class="hubungi">
				<input type="text" placeholder="Nama" class="input">
				<div class="clearfix pt10"></div>
				<input type="text" placeholder="Email" class="input">
				<div class="clearfix pt10"></div>
				<textarea name="" id="" class="input textarea" placeholder="Pesan"></textarea>
				<div class="clearfix pt5"></div>
				<input type="image" src="img/btn_kirim.png">
			</form>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
	
</body>
</html>
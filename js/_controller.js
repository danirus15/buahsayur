$(document).ready(function() {
  $('#nav_menu').click(function() {
    $('#nav').slideDown(200);
    $('#nav_menu').hide();
    $('#nav_menu_close').show();
  });
  $('#nav_menu_close').click(function() {
    $('#nav').slideUp(200);
    $('#nav_menu').show();
    $('#nav_menu_close').hide();
  });

  $(".imgLiquid").imgLiquid({fill:true});

});

$("#accordion .subjudul").click(function (){
    if($(this).find("a.item").hasClass("popularOver")){
      $("#accordion .subjudul ul").slideUp();
      $("#accordion .subjudul a").removeClass("popularOver");
    }
    else {
      $("#accordion .subjudul ul").slideUp();
      $("#accordion .subjudul a").removeClass("popularOver");
      $(this).find("ul").slideDown(200);
      $(this).find("a.item").addClass("popularOver");
    }
    //alert();
});

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});

$(function() {
  $('#carousel').carouFredSel({
    responsive: true,
    items: {
      width: 320,
      visible: {
        min: 1,
        max: 4
      }
    },
    auto:true,
    prev:'#slide2_prev',
    next:'#slide2_next',
    pagination  : "#pagnation2",
    swipe: {
                  onMouse: true,
                  onTouch: true
              }
  });

  $('#hl').carouFredSel({
    responsive: true,
    pagination  : "#pagnation",
     auto:true,
    items: {
      visible: {
        min: 1,
        max: 1
      }
    },
    scroll: {
      duration:1700
    },
    swipe: {
                  onMouse: true,
                  onTouch: true
              }
  });
});


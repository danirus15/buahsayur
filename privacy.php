<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<?php include "includes/head.php";?>

<body>
<?php include "includes/header.php";?>
<div class="header_page header_page2">
	<div class="container">
	</div>
</div>
<div class="clearfix pt30"></div>
<div class="container">
	<div class="breadcrumb">
		<img src="img/ico_home.jpg" alt="">
		<span>
			/ Terms & Condition
		</span>
	</div>
	<div class="clearfix pt10"></div>
	<div class="title_page"><h1>Terms & Condition</h1></div>
	<div class="text_detail">
		<p>Selamat datang di Elevenia di www.elevenia.co.id ("Web Site"). Elevenia adalah layanan jasa online yang dioperasikan oleh Elevenia, yang menyediakan layanan bagi pengguna dengan akses untuk menemukan dan membeli berbagai produk dari penjual terdaftar melalui multiple electronic interface termasuk situs Elevenia, aplikasi mobile, aplikasi web atau "widget", dan aplikasi lainnya ("Layanan"). Web Site ini tersedia untuk anda sesuai dengan Perjanjian ini ("Perjanjian"), yang berlaku efektif sampai diakhiri oleh Elevenia ("Elevenia" dan/atau "kami").</p>
		<br>
		Silahkan baca Perjanjian berikut dengan seksama sebelum menggunakan Web Site ini, karena penggunaan Web Site tunduk pada Perjanjian ini, seluruh ketentuan hukum yang berlaku, dan merupakan penerimaan Anda atas Perjanjian ini berikut Ketentuan lainnya yang ditetapkan oleh Elevenia. Jika Anda tidak setuju untuk terikat dengan Perjanjian ini, maka anda tidak diperbolehkan untuk menggunakan Web Site atau mengunduh materi dari Web Site.
		<br><br>
		Dengan mendaftar, mengakses atau menggunakan Layanan, Anda menyatakan bahwa Anda telah membaca, memahami dan setuju untuk terikat oleh Perjanjian dan/atau Ketentuan lainnya yang diberlakukan oleh Elevenia, baik apabila Anda seorang Pengguna Terdaftar dari Layanan ataupun bukan. Anda hanya diizinkan untuk menggunakan Layanan, terlepas dari apakah akses yang Anda lakukan adalah disengaja ataupun tidak, jika Anda setuju untuk mematuhi seluruh ketentuan hukum yang berlaku dan Perjanjian ini dan Ketentuan lainnya diberlakukan oleh Elevenia. Elevenia berhak, atas Ketentuannya sendiri, untuk mengubah, memodifikasi, menambah, atau menghapus bagian dari Perjanjian ini setiap saat tanpa pemberitahuan lebih lanjut. Jika Elevenia melakukan hal ini, Elevenia akan mengumumkan perubahan Perjanjian pada Web Site ini. Dengan terus menggunakan Web site ini setelah adanya perubahan tersebut merupakan tanda penerimaan Anda terhadap revisi Perjanjian dan Ketentuan lainnya yang diberlakukan oleh Elevenia.
		<br><br>
		Jika Anda tidak mematuhi Perjanjian dan/atau Ketentuan lainnya yang diberlakukan setiap saat oleh Elevenia, Elevenia berhak untuk segera membatalkan atau menghentikan akses Anda ke Layanan (atau bagian daripadanya) dan atau akun pengguna Anda, apabila ada. Elevenia, dalam Ketentuannya sendiri, dan tanpa pemberitahuan atau kewajiban sebelumnya, Elevenia dapat menghentikan, memodifikasi atau mengubah setiap aspek dari Layanan, termasuk, namun tidak terbatas pada : (i) membatasi waktu ketersediaan Layanan, (ii) membatasi jumlah penggunaan yang diizinkan, dan (iii) membatasi atau mengakhiri hak setiap pengguna untuk menggunakan Layanan. Anda setuju bahwa setiap penghentian atau pembatalan akses Anda ke, atau penggunaan atas, Layanan dapat dilakukan tanpa pemberitahuan sebelumnya. Jika Anda tidak mematuhi ketentuan Perjanjian ini dan/atau Ketentuan lain yang ditentukan oleh Elevenia, Anda setuju bahwa Elevenia dapat dengan segera menghentikan atau menghapus akun pengguna, jika ada, dan semua informasi terkait dan/atau file di Akun Pengguna anda dan/atau setiap akses lebih lanjut terhadap informasi tersebut dan/atau file dan/atau Layanan atau bagian daripadanya. Selanjutnya, Anda setuju bahwa Elevenia tidak bertanggung jawab kepada Anda atau pihak ketiga atas pemutusan atau pembatalan akses Anda ke, atau penggunaan atas, Layanan. Anda mengakui bahwa hanya hak Anda sehubungan dengan ketidakpuasan atas modifikasi atau penghentian Layanan, atau Ketentuan atau praktik oleh Elevenia dalam menyediakan Layanan, termasuk tapi tidak terbatas pada perubahan apapun pada konten, adalah untuk berhenti menggunakan Layanan dan membatalkan atau menghentikan keanggotaan Anda atau akun Pengguna Terdaftar, sebagaimana berlaku.
		<br><br>
		<h3>1. Konten</h3>
		Web Site dan konten daripadanya, <a href="#">termasuk, namun tidak terbatas pada</a>, semua teks, ilustrasi, file, gambar, perangkat lunak, data, logo, tombol ikon, grafik, legenda, foto, resep, teknologi, musik, audio atau klip video, dan fitur interaktif (secara kolektif, "Konten Web Site") dan semua hak kekayaan intelektual yang ada padanya, dimiliki oleh Elevenia, lisensi, atau keduanya. Selain itu, semua merek dagang, merek layanan, dan nama dagang yang mungkin muncul di Web Site kami dimiliki oleh Elevenia, pemegang lisensinya, atau keduanya. Kecuali untuk hak penggunaan terbatas yang diberikan kepada Anda dalam Perjanjian ini, Anda tidak akan memperoleh hak, hak milik atau kepentingan atas Web Site atau Konten Web Site. Setiap hak yang tidak diberikan dalam Perjanjian ini telah secara tegas dilindungi.
		<br><br>
		<h3>2. Warna</h3>
		Anda mengakui bahwa warna sebenarnya dari produk sebagaimana terlihat di Web Site tergantung pada monitor komputer Anda. Elevenia telah menggunakan upaya terbaik untuk memastikan warna dalam foto-foto yang ditampilkan di Web Site muncul seakurat mungkin, tetapi tidak dapat menjamin bahwa penampilan warna pada Web Site akan akurat.
		<br><br>
		<h3>3. Penggunaan Web Site</h3>
		Akses ke Web Site termasuk, tapi tidak terbatas pada, Konten Web Site disediakan untuk informasi dan penggunaan non âkomersial untuk pribadi . Saat menggunakan Web Site, Anda setuju untuk mematuhi seluruh ketentuan hukum yang berlaku, termasuk tapi tidak terbatas pada, ketentuan hukum mengenai hak cipta. Anda dapat mengakses dan melihat Konten Web Site pada komputer atau perangkat lain dan , kecuali dinyatakan lain dalam Perjanjian ini atau Ketentutan lain yang ditetapkan oleh Elevenia atau pada Web Site, membuat salinan tunggal atau mencetak konten Web Site terbatas hanya untuk penggunaan pribadi. Penggunaan Anda atas Web Site tidak memberikan Anda kepemilikan dari setiap Konten Web Site yang anda akses melalui Web Site. Elevenia dan Konten Web Site atau bagian daripadanya, tidak boleh didistribusikan, dipublikasikan, direproduksi, diduplikasi, disalin, ditransmisikan, dibongkar, dijual, atau dieksploitasi untuk tujuan komersial kecuali secara tegas diizinkan dalam Perjanjian ini atau dalam izin tertulis yang diberikan oleh Elevenia.
		Akses ke Web Site dengan menggunakan perangkat seluler mungkin dikenakan biaya tambahan tergantung pada operator selular, dan/atau rencana berlangganan. Biaya tambahan ini ditanggung sepenuhnya oleh Anda dan Elevenia tidak bertanggung jawab untuk biaya tersebut.
	</div>
	<div class="clearfix"></div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
	
</body>
</html>
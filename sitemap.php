<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"> 
<?php include "includes/head.php";?>

<body>
<?php include "includes/header.php";?>
<div class="header_page header_page2">
	<div class="container">
	</div>
</div>
<div class="clearfix pt30"></div>
<div class="container">
	<div class="breadcrumb">
		<img src="img/ico_home.jpg" alt="">
		<span>
			/ Site Map
		</span>
	</div>
	<div class="clearfix pt10"></div>
	<div class="title_page"><h1>Site Map</h1></div>
	<div class="sitemap_list">
		<h2>A. Fakta Buah & Sayur</h2>
		<h3>1 Pentingnya Buah & Sayur</h3>
		<h4><a href="#">Buah & Sayur itu Penting untuk Pertumbuhan Sehat si Kecil.</a></h4>
		<h3>2 BuahSayur & perkembangan si Kecil</h3>
		<h4><a href="#">Kapan Bunda Mulai Memberikan si Kecil Buah dan Sayur?</a></h4>
		<div class="clearfix pt30"></div>
		<h2>B. Tips Buah Sayur & Si Kecil</h2>
		<h3>1 Sesuai Usia si Kecil</h3>
		<h4><a href="#">Buah & Sayur itu Penting untuk Pertumbuhan Sehat si Kecil.</a></h4>
		<h3>2 Pesan Ahli untuk Bunda</h3>
		<h4><a href="#">Kapan Bunda Mulai Memberikan si Kecil Buah dan Sayur?</a></h4>
		<h4> <a href="">Tips untuk Menjaga Kualitas Buah dan Sayur.</a></h4>
		<h3>3 Tips Aktifitas : </h3>
		<h4><a href="#">Dapatkan tips dari kami agar si Kecil mulai makan buah dan sayur</a></h4>
		<h4>   <a href="#">Temukan tips para ahli kami agar si Kecil makan buah dan sayur</a></h4>
		<h4> <a href="#"> Membiasakan diri dengan teman-teman baru</a></h4>
		<h4>  <a href="#">Bunda adalah motivasi bagi si Kecil</a></h4>
		<h4>  <a href="#">Yuk bermain bersama buah dan sayur</a></h4>
		<div class="clearfix pt30"></div>
		<h2>C. Panduan Diet Seimbang</h2>
		<h3>1. Asupan Gizi Seimbang </h3>
		<h4> <a href="#"> Buah dan Sayur Harus Mencakup 50% dalam Konsumsi Harian si Kecil</a></h4>
	</div>
	<div class="sitemap_list">
		<h2>Hubungi Kami</h2>
		<h4><a href="#">Hubungi kami</a></h4>
		<div class="clearfix pt30"></div>
		<h2>Tentang </h2>
		<h4><a href="#">Terms & Conditions </a></h4>
		<h4><a href="#">Privacy Policy </a></h4>
		<h4><a href="#">Site Map</a></h4>
	</div>

	</div>
	<div class="clearfix"></div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
	
</body>
</html>
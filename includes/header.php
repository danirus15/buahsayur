<div id="header">
	<div class="container">
		<a href="index.php" class="logo pic"><h1><img src="img/logo_n.png" alt="Sayur Buah"></h1></a>
		<div id="nav">
			<div class="login_area">Hi pengunjung, 
				<a href="#">daftar</a> / 
				<a href="#">login</a>
			</div>
			<div class="clearfix"></div>
			<div class="search">
				<form action="search.php" method="post">
					<input type="text" class="search_input">
					<input type="image" src="img/ico_search.png" class="search_btn">
				</form>
			</div>
			<div id="menu">
				<ul class="list_accordion">
					<li class="subjudul">
						<a href="#fakta" class="item popular" rel="popular">Fakta Buah & Sayur</a>
					</li>
					<li class="subjudul">
						<a href="#panduan" class="item popular" rel="popular">Panduan Gizi Seimbang</a>
					</li>
					<li class="subjudul">
						<a href="#tips" class="item popular" rel="popular">Tips Buah, Sayur dan si Kecil</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		
	</div>
	<div id="nav_menu">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div id="nav_menu_close"><span>+</span></div>
</div>
<div class="clearfix"></div>

<a href="#top" class="scroll_up"><img src="img/btn_up.png" alt="back to top"></a>